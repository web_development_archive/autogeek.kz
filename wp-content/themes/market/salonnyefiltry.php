<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Market
 * Template Name: salonnyefiltry
 */
get_header(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".proizvoditel").change(function() {
			this.form.submit();
		});
		$(".obem").change(function() {
			this.form.submit();
		});
		$(".dvigatel").change(function() {
			this.form.submit();
		});
		$(".sostav").change(function() {
			this.form.submit();
		});
		$(".vyazkost").change(function() {
			this.form.submit();
		});
		$(".sorting").change(function(){
			this.form.submit();
		});
		$(".taktnistdrivatelya").change(function() {
			this.form.submit();
		});
		$("#first_price_motmasla").change(function() {
			this.form.submit();
		});
		$("#second_price_motmasla").change(function() {
			$("#matormaslaidform").submit();
		});

		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 40000,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['price_first'])) echo $_GET['price_first']; else echo 0; ?>, <?php if(isset($_GET['price_second'])) echo $_GET['price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#amount1" ).val(ui.values[ 0 ]+" тенге");
				$( "#amount2" ).val(ui.values[ 1 ]+" тенге");
				$( "#amount1_1" ).val(ui.values[ 0 ]);
				$( "#amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#amount1" ).val($( "#slider-range" ).slider( "values", 0 )+" тенге");
		$( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 )+" тенге");
		$( "#amount1_1" ).val($( "#slider-range" ).slider( "values", 0 ));
		$( "#amount2_2" ).val($( "#slider-range" ).slider( "values", 1 ));
	});

</script>

<style>
	ul li {
		list-style: none;
	}
	header.entry-header {
		display: none;
	}
</style>
<form name="search" action="http://autogeek.kz/%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D1%8B/%D1%81%D0%B0%D0%BB%D0%BE%D0%BD%D0%BD%D1%8B%D0%B5-%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D1%8B/" method="get" id="matormaslaidform">
<div id="" class="content-area col-md-4">

		<ul>
			<li class="cataloglist_header">Цена:</li>
			<li>
				<div class="interes_1 polzunok">
					<div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
					<input name="price_first" type="hidden" id="amount1_1" readonly="" style="border:0;">
					<input name="price_second" type="hidden" id="amount2_2" readonly="" style="border:0;">
					<input type="text" id="amount1" readonly="" style="border:0;">
					<input type="text" id="amount2" readonly="" style="border:0;">
				</div>
			</li>
			<hr />
		</ul>
		<ul class="sidebar-catalogList">
			<?php
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Производитель';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['proizvoditel'])) && (in_array($bla, $_GET['proizvoditel']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="proizvoditel" id="squaredThree_' . $bla . '" name="proizvoditel[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}
			?>
			<hr />
		</ul>
	</div>
	<div id="primary" class="content-area col-md-8">
		<main id="main" class="site-main" role="main">
			<div class="header_title_products">
				<h3>Салонные фильтры</h3>
				<div class="sorting">
					<span>Сортировать по:</span>
					<select class="sorting" name="order" style="    border-radius: 5px;">
						<option value="none" <?php if($_GET['order']=="none") echo 'selected';?> >Приоритету</option>
						<option value="price_desc" <?php if($_GET['order']=="price_desc") echo 'selected';?> >Цене - убывание</option>
						<option value="price_asc" <?php if($_GET['order']=="price_asc") echo 'selected';?> >Цене - возростание</option>
						<option value="title_desc" <?php if($_GET['order']=="title_desc") echo 'selected';?> >Названию - Я-А Z-A</option>
						<option value="title_asc" <?php if($_GET['order']=="title_asc") echo 'selected';?> >Названию - A-Z А-Я</option>
					</select>
				</div>
			</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
				?>


			<?php endwhile; // end of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

</form>
<?php get_footer(); ?>
