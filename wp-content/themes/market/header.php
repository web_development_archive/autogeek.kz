<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Market
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
    <meta name="viewport" content="width=1170, user-scalable=yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=1170, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
<script type="text/javascript" src="/callme/js/callme.js" charset='utf-8'></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
	<style>
		.advantage {
			position: relative;
			margin-top: 40px;
			border-radius: 10px;
		}
		.advantage h1 {
			font-weight: normal;
			font-size: 24px;
			text-transform: uppercase;
			position: absolute;
			padding: 0 10px;
			display: inline-block;
			left: 50%;
			top: -13px;
			margin-left: -101px;
		}
		.advantage-list {
			padding-top: 50px;
			text-align: center;
		}
		.advantage-list ul {
			display: inline-block;
		}
		.advantage-list li {
			float: left;
			width: 250px;
		}
		.advantage-img {
			height: 110px;
			overflow: hidden;
		}
		.advantage h2 {
			margin-top: 10px;
			font-size: 14px;
			font-weight: normal;
		}
		.advantage h2 a {
			text-transform: uppercase;
			color: #3c3c3c;
		}
		.shn ul li img {
			width: 24px;
		}
		.img img {
			width: 45px;
			margin: 0 10px;
		}
		.counters {
			width: auto!important;
			display: block;
			margin: 10px!important;
		}
		footer#colophon {
			display: none!important;
		}

		#social-icons {
			float: right;
			text-align: right;
			width: auto!important;
			margin:40px 0 0 0;
			display: inline-block;
			vertical-align: top;
		}
		.callme_viewform {
			background: #2c4e86;
			width: 165px;
			box-shadow: 0px 3px 14px #252525;
			padding: 9px;
			cursor: pointer;
			border-radius: 10px;
			text-transform: uppercase;
			float: right;
			color: white;
			font-size: 15px;
			margin: 25px 0 0 0;
			font-weight: 600;
		}
		#site-navigation ul > li > a {
			padding-top: 0px!important;
			width: 145px;
		}
		img.menu_image_item {
			padding-top: 20px;
		}
		#site-navigation ul ul
		{
			top: 9em!important;
		}
		#site-navigation li:hover > a
		{
			padding-top: 0px!important;
		}
		#callme { display: none!important;}
		.sidebar_r > img {
  float: left;
  width: 45px;
  margin: 0px 10px 0px 0;
}
.sidebar_r >div {
  display: inline;
  width: 52px;
  line-height: 60px;
}
.sidebar_r {
  display: inline-block;
}
	</style>
	<script>//
		$( document ).ready(function() {
			$("#menu-item-686").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/аккум.png" alt>');
			$("#menu-item-684").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/подборфильтров.png" alt>');
			$("#menu-item-685").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/оптовымпокуп.png" alt>');
			$("#menu-item-54").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/масло.png" alt>');
			$("#menu-item-208").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/автохимия.png" alt>');
			$("#menu-item-230").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/фильры.png" alt>');
			$("#menu-item-218").prepend('<img class="menu_image_item" src="http://autogeek.kz/wp-content/uploads/2015/04/акссесуары.png" alt>');



			var count = 0;
			$(".mobileMenu option").each(function( index ) {
				alert($( this ).attr("value"));
				if($( this ).attr("value")=="undefined")
				{
					alert(count);
					count++;
					if(count==1)
					{
						$( this ).attr("value","http://autogeek.kz/%d0%b0%d0%b2%d1%82%d0%be-%d0%b0%d0%ba%d1%81%d0%b5%d1%81%d1%81%d1%83%d0%b0%d1%80%d1%8b/%d0%b0%d0%ba%d0%ba%d1%83%d0%bc%d1%83%d0%bb%d1%8f%d1%82%d0%be%d1%80%d1%8b/");
					}else if(count==2)
					{
						$( this ).attr("value","http://autogeek.kz/%d0%bf%d0%be%d0%b4%d0%b1%d0%be%d1%80-%d0%bc%d0%b0%d1%81%d0%bb%d0%b0-%d0%b8-%d1%84%d0%b8%d0%bb%d1%8c%d1%82%d1%80%d0%be%d0%b2/");
					}else if(count==3)
					{
						$( this ).attr("value","http://autogeek.kz/%d0%be%d0%bf%d1%82%d0%be%d0%b2%d1%8b%d0%bc-%d0%bf%d0%be%d0%ba%d1%83%d0%bf%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/");
					}
				};


				});


				$( "#social-icons a" ).each(function( index ) {
					if($( this ).attr("title")=="Twitter")
					{
						$( this ).attr("title","Vk");
					};
					if($( this ).attr("title")=="Google Plus")
					{
						$( this ).attr("title","Одноклассники");
					};
				});
				$( "#slider-range" ).slider({
					range: true,
					min: 0,
					max: 30000,
					stop: function( event, ui ) {
						$("#matormaslaidform").submit();
					},
					values: [ 10000, 20000 ],
					slide: function( event, ui ) {
						$( "#amount" ).val( "т " + ui.values[ 0 ] + " - т " + ui.values[ 1 ] );
						$("#first_price_motmasla").val(ui.values[ 0 ]);
						$("#second_price_motmasla").val(ui.values[ 1 ]);
					}
				});
				$( "#amount" ).val( "т " + $( "#slider-range" ).slider( "values", 0 ) +
				" - т " + $( "#slider-range" ).slider( "values", 1 ) );
		});


	</script>
</head>

<body <?php body_class(); ?>>
<div id="parallax-bg"></div>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<div id="top-bar">
		<div class="container">
			
			<div id="top-search-form" class="col-md-12">
				<?php get_template_part('searchform', 'top'); ?>
			</div>
			
			<div id="top-navigation" class="col-md-9">
				<?php wp_nav_menu( array( 'theme_location' => 'top' ) ); ?>
			</div>	

			 <div class="top-icons-container col-md-3">
				
				<?php if (class_exists('woocommerce')) :
				?>
					<div class="top-cart-icon">
						<i class="fa"><img style="  width: 30px;   padding: 3px; " src="http://autogeek.kz/wp-content/uploads/2015/04/23258.png" alt></i>
						<?php
						 global $woocommerce; ?>
	 
					<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d товар', '%d товаров', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
					</div>
				<?php endif; ?>	
					<div class="top-search-icon">
						<i class="fa"><img style="  width: 20px;" src="http://autogeek.kz/wp-content/uploads/2015/04/70376.png" alt></i>
					</div>
			</div>
		
		</div>
	</div><!--#top-bar-->
	
	<div id="header-top">
		<header id="masthead" class="site-header row container" role="banner">
			<div class="site-branding col-md-4 col-xs-12">
			<?php if((of_get_option('logo', true) != "") && (of_get_option('logo', true) != 1) ) { ?>
				<h1 class="site-title logo-container"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<?php
				echo "<img class='main_logo' src='".of_get_option('logo', true)."' title='".esc_attr(get_bloginfo( 'name','display' ) )."'></a></h1>";	
				}
			else { ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> 
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			<?php	
			}
			?>
			</div>

			<p style="  width: 250px; display: inline-block;"><br style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 16px;"><span style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 16px;">Телефоны(WhatsApp):</span><br style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 25px;"><span style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 16px; font-size:20px;">+7 (708) 760-30-25</span><br style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 16px;"><span style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 16px;  font-size:20px;">+7 (708) 760-30-26</span><br style="color: #003366; font-family: Helvetica, Arial, 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 16px;"></p>

			<div style="    display: inline-block;">
				<section class="callme_viewform">Заказать звонок</section>
			</div>
			<?php
			
			if ( of_get_option('facebook',true) == 1 ) :
				 get_template_part('defaults/social');
			else :
				get_template_part('social', 'default');
			endif; ?>
			
		</header><!-- #masthead -->
	</div>
	
	<div id="header-2">
		<div class="container">
		<div class="default-nav-wrapper col-md-11 col-xs-12" style="    margin: 0 0 0 50px;"> 	
		   <nav id="site-navigation" class="main-navigation" role="navigation">
	         <div id="nav-container">
				<h1 class="menu-toggle"></h1>
				<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'market' ); ?>"><?php _e( 'Skip to content', 'market' ); ?></a></div>
	
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	          </div>  
			</nav><!-- #site-navigation -->
		  </div>
		  
		<div id="top-search" class="col-md-1 col-xs-12">
			<?php // get_search_form(); ?>
		</div>
		</div>
	</div>

	
	<?php
			if ( of_get_option('slide1',true) == 1 ) :
				 get_template_part('defaults/slider');
			else :
				get_template_part('slider', 'nivo');
			endif; ?>
		<div id="content" class="site-content container row clearfix clear">
		<div class="col-md-12"> 

