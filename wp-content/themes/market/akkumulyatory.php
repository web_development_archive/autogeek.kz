<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Market
 * Template Name: akkumulyatory
 */
get_header(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".proizvoditel").change(function() {
			this.form.submit();
		});
		$(".emkost").change(function() {
			this.form.submit();
		});
        $(".sorting").change(function(){
            this.form.submit();
        });

		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 58250,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['price_first'])) echo $_GET['price_first']; else echo 0; ?>, <?php if(isset($_GET['price_second'])) echo $_GET['price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#amount1" ).val(ui.values[ 0 ]+" тенге");
				$( "#amount2" ).val(ui.values[ 1 ]+" тенге");
				$( "#amount1_1" ).val(ui.values[ 0 ]);
				$( "#amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#amount1" ).val($( "#slider-range" ).slider( "values", 0 )+" тенге");
		$( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 )+" тенге");
		$( "#amount1_1" ).val($( "#slider-range" ).slider( "values", 0 ));
		$( "#amount2_2" ).val($( "#slider-range" ).slider( "values", 1 ));


		$( "#dlina_slider-range" ).slider({
			range: true,
			min: 0,
			max: 360,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['dlina_price_first'])) echo $_GET['dlina_price_first']; else echo 0; ?>, <?php if(isset($_GET['dlina_price_second'])) echo $_GET['dlina_price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#dlina_amount1" ).val(ui.values[ 0 ]+" см");
				$( "#dlina_amount2" ).val(ui.values[ 1 ]+" см");
				$( "#dlina_amount1_1" ).val(ui.values[ 0 ]);
				$( "#dlina_amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#dlina_amount1" ).val($( "#dlina_slider-range" ).slider( "values", 0 )+" см");
		$( "#dlina_amount2" ).val($( "#dlina_slider-range" ).slider( "values", 1 )+" см");
		$( "#dlina_amount1_1" ).val($( "#dlina_slider-range" ).slider( "values", 0 ));
		$( "#dlina_amount2_2" ).val($( "#dlina_slider-range" ).slider( "values", 1 ));

		$( "#width_slider-range" ).slider({
			range: true,
			min: 0,
			max: 360,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['width_price_first'])) echo $_GET['width_price_first']; else echo 0; ?>, <?php if(isset($_GET['width_price_second'])) echo $_GET['width_price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#width_amount1" ).val(ui.values[ 0 ]+" см");
				$( "#width_amount2" ).val(ui.values[ 1 ]+" см");
				$( "#width_amount1_1" ).val(ui.values[ 0 ]);
				$( "#width_amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#width_amount1" ).val($( "#width_slider-range" ).slider( "values", 0 )+" см");
		$( "#width_amount2" ).val($( "#width_slider-range" ).slider( "values", 1 )+" см");
		$( "#width_amount1_1" ).val($( "#width_slider-range" ).slider( "values", 0 ));
		$( "#width_amount2_2" ).val($( "#width_slider-range" ).slider( "values", 1 ));


		$( "#height_slider-range" ).slider({
			range: true,
			min: 0,
			max: 360,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['height_price_first'])) echo $_GET['height_price_first']; else echo 0; ?>, <?php if(isset($_GET['height_price_second'])) echo $_GET['height_price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#height_amount1" ).val(ui.values[ 0 ]+" см");
				$( "#height_amount2" ).val(ui.values[ 1 ]+" см");
				$( "#height_amount1_1" ).val(ui.values[ 0 ]);
				$( "#height_amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#height_amount1" ).val($( "#height_slider-range" ).slider( "values", 0 )+" см");
		$( "#height_amount2" ).val($( "#height_slider-range" ).slider( "values", 1 )+" см");
		$( "#height_amount1_1" ).val($( "#height_slider-range" ).slider( "values", 0 ));
		$( "#height_amount2_2" ).val($( "#height_slider-range" ).slider( "values", 1 ));
	});

</script>

<style>
	ul li {
		list-style: none;
	}
	header.entry-header {
		display: none;
	}
</style>
<form name="search" action="http://autogeek.kz/%D0%B0%D0%B2%D1%82%D0%BE-%D0%B0%D0%BA%D1%81%D0%B5%D1%81%D1%81%D1%83%D0%B0%D1%80%D1%8B/%D0%B0%D0%BA%D0%BA%D1%83%D0%BC%D1%83%D0%BB%D1%8F%D1%82%D0%BE%D1%80%D1%8B/" method="get" id="matormaslaidform">
<div id="" class="content-area col-md-4">

		<ul>
			<li class="cataloglist_header">Цена:</li>
			<li>
				<div class="interes_1 polzunok">
					<div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
					<input name="price_first" type="hidden" id="amount1_1" readonly="" style="border:0;">
					<input name="price_second" type="hidden" id="amount2_2" readonly="" style="border:0;">
					<input type="text" id="amount1" readonly="" style="border:0;">
					<input type="text" id="amount2" readonly="" style="border:0;">
				</div>
			</li>
			<hr />
		</ul>

    <ul class="sidebar-catalogList">
        <li class="cataloglist_header">Емкость, Ач:</li>
        <li style="    margin: 0 19px 0 0;">
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("10", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="10" class="emkost" id="squaredThree_10" name="emkost[]">
                <label for="squaredThree_10"></label>
                <span>10</span>
            </div>
        </li><li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("60", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="60" class="emkost" id="squaredThree_60" name="emkost[]">
                <label for="squaredThree_60"></label>
                <span>60</span>
            </div>
        </li>

        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("14", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="14" class="emkost" id="squaredThree_14" name="emkost[]">
                <label for="squaredThree_14"></label>
                <span>14</span>
            </div>
        </li>

        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("63", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="63" class="emkost" id="squaredThree_63" name="emkost[]">
                <label for="squaredThree_63"></label>
                <span>63</span>
            </div>
        </li>

        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("19", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="19" class="emkost" id="squaredThree_19" name="emkost[]">
                <label for="squaredThree_19"></label>
                <span>19</span>
            </div>
        </li>

        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("70", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="70" class="emkost" id="squaredThree_70" name="emkost[]">
                <label for="squaredThree_70"></label>
                <span>70</span>
            </div>
        </li>

        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("20", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="20" class="emkost" id="squaredThree_20" name="emkost[]">
                <label for="squaredThree_20"></label>
                <span>20</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("74", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="74" class="emkost" id="squaredThree_74" name="emkost[]">
                <label for="squaredThree_74"></label>
                <span>74</span>
            </div>
        </li>

        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("25", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="25" class="emkost" id="squaredThree_25" name="emkost[]">
                <label for="squaredThree_25"></label>
                <span>25</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("95", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="95" class="emkost" id="squaredThree_95" name="emkost[]">
                <label for="squaredThree_95"></label>
                <span>95</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['emkost'])) echo (in_array("30", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="30" class="emkost" id="squaredThree_30" name="emkost[]">
                <label for="squaredThree_30"></label>
                <span>30</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("100", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="100" class="emkost" id="squaredThree_100" name="emkost[]">
                <label for="squaredThree_100"></label>
                <span>100</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input  <?php  if(isset($_GET['emkost'])) echo (in_array("45", $_GET['emkost']))?"checked":"";  ?> type="checkbox" value="45" class="emkost" id="squaredThree_45" name="emkost[]">
                <label for="squaredThree_45"></label>
                <span>45</span>
            </div>
        </li>




        <li>
            <div class="squaredThree cat_div">
                <input type="checkbox" value="180" class="emkost" id="squaredThree_180" name="emkost[]">
                <label for="squaredThree_180"></label>
                <span>180</span>
            </div>
        </li>
        <hr>
    </ul>
		<!--<ul class="sidebar-catalogList">
			<?php
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Емкость, Ач:';
			echo '</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['emkost'])) && (in_array($bla, $_GET['emkost']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="emkost" id="squaredThree_' . $bla . '" name="emkost[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}
			?>
			<hr />
		</ul>-->

	<ul>
		<li class="cataloglist_header">Длина:</li>
		<li>
			<div class="interes_1 polzunok">
				<div id="dlina_slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
				<input name="dlina_price_first" type="hidden" id="dlina_amount1_1" readonly="" style="border:0;">
				<input name="dlina_price_second" type="hidden" id="dlina_amount2_2" readonly="" style="border:0;">
				<input type="text" id="dlina_amount1" readonly="" style="border:0;">
				<input type="text" id="dlina_amount2" readonly="" style="border:0;">
			</div>
		</li>
		<hr />
	</ul>

	<ul>
		<li class="cataloglist_header">Ширина:</li>
		<li>
			<div class="interes_1 polzunok">
				<div id="width_slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
				<input name="width_price_first" type="hidden" id="width_amount1_1" readonly="" style="border:0;">
				<input name="width_price_second" type="hidden" id="width_amount2_2" readonly="" style="border:0;">
				<input type="text" id="width_amount1" readonly="" style="border:0;">
				<input type="text" id="width_amount2" readonly="" style="border:0;">
			</div>
		</li>
		<hr />
	</ul>

	<ul>
		<li class="cataloglist_header">Высота:</li>
		<li>
			<div class="interes_1 polzunok">
				<div id="height_slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
				<input name="height_price_first" type="hidden" id="height_amount1_1" readonly="" style="border:0;">
				<input name="height_price_second" type="hidden" id="height_amount2_2" readonly="" style="border:0;">
				<input type="text" id="height_amount1" readonly="" style="border:0;">
				<input type="text" id="height_amount2" readonly="" style="border:0;">
			</div>
		</li>
		<hr />
	</ul>

	</div>
	<div id="primary" class="content-area col-md-8">
		<main id="main" class="site-main" role="main">
			<div class="header_title_products">
				<h3>Аккумуляторы</h3>
				<div class="sorting">
					<span>Сортировать по:</span>
					<select class="sorting" name="order" style="    border-radius: 5px;">
						<option value="none" <?php if($_GET['order']=="none") echo 'selected';?> >Приоритету</option>
						<option value="price_desc" <?php if($_GET['order']=="price_desc") echo 'selected';?> >Цене - убывание</option>
						<option value="price_asc" <?php if($_GET['order']=="price_asc") echo 'selected';?> >Цене - возростание</option>
						<option value="title_desc" <?php if($_GET['order']=="title_desc") echo 'selected';?> >Названию - Я-А Z-A</option>
						<option value="title_asc" <?php if($_GET['order']=="title_asc") echo 'selected';?> >Названию - A-Z А-Я</option>
					</select>
				</div>
			</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
				?>


			<?php endwhile; // end of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

</form>
<?php get_footer(); ?>
