<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Market
 * Template Name: matornyemasla
 */
get_header(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".proizvoditel").change(function() {
			this.form.submit();
		});
		$(".obem").change(function() {
			this.form.submit();
		});
		$(".dvigatel").change(function() {
			this.form.submit();
		});
		$(".sostav").change(function() {
			this.form.submit();
		});
		$(".vyazkost").change(function() {
			this.form.submit();
		});
		$(".sorting").change(function(){
			this.form.submit();
		});
		$(".taktnistdrivatelya").change(function() {
			this.form.submit();
		});
		$("#first_price_motmasla").change(function() {
			this.form.submit();
		});
		$("#second_price_motmasla").change(function() {
			$("#matormaslaidform").submit();
		});

		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 12500,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['price_first'])) echo $_GET['price_first']; else echo 0; ?>, <?php if(isset($_GET['price_second'])) echo $_GET['price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#amount1" ).val(ui.values[ 0 ]+" тенге");
				$( "#amount2" ).val(ui.values[ 1 ]+" тенге");
				$( "#amount1_1" ).val(ui.values[ 0 ]);
				$( "#amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#amount1" ).val($( "#slider-range" ).slider( "values", 0 )+" тенге");
		$( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 )+" тенге");
		$( "#amount1_1" ).val($( "#slider-range" ).slider( "values", 0 ));
		$( "#amount2_2" ).val($( "#slider-range" ).slider( "values", 1 ));
	});

</script>

<style>
	ul li {
		list-style: none;
	}
	header.entry-header {
		display: none;
	}
</style>
<form name="search" action="http://autogeek.kz/%D0%BC%D0%B0%D1%81%D0%BB%D0%B0-%D0%B8-%D0%B6%D0%B8%D0%B4%D0%BA%D0%BE%D1%81%D1%82%D0%B8/%D0%BC%D0%BE%D1%82%D0%BE%D1%80%D0%BD%D1%8B%D0%B5-%D0%BC%D0%B0%D1%81%D0%BB%D0%B0/" method="get" id="matormaslaidform">
<div id="" class="content-area col-md-4">

		<ul>
			<li class="cataloglist_header">Цена:</li>
			<li>
				<div class="interes_1 polzunok">
					<div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
					<input name="price_first" type="hidden" id="amount1_1" readonly="" style="border:0;">
					<input name="price_second" type="hidden" id="amount2_2" readonly="" style="border:0;">
					<input type="text" id="amount1" readonly="" style="border:0;">
					<input type="text" id="amount2" readonly="" style="border:0;">
				</div>
			</li>
			<hr />
		</ul>
		<ul class="sidebar-catalogList">
			<?php
            /*
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Производитель';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['proizvoditel'])) && (in_array($bla, $_GET['proizvoditel']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="proizvoditel" id="squaredThree_' . $bla . '" name="proizvoditel[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}*/
			?>
            <li class="cataloglist_header">Производитель:</li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Mobil", $_GET['proizvoditel']))?"checked":"";  ?> type="checkbox" value="Mobil" class="proizvoditel" id="squaredThree_Mobil" name="proizvoditel[]">
                    <label for="squaredThree_Mobil"></label>
                    <span>Mobil</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Castrol", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="Castrol" class="proizvoditel" id="squaredThree_Castrol" name="proizvoditel[]">
                        <label for="squaredThree_Castrol"></label>
                        <span>Castrol</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("BP Visco", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="BP Visco" class="proizvoditel" id="squaredThree_BP Visco" name="proizvoditel[]">
                    <label for="squaredThree_BP Visco"></label>
                    <span>BP Visco</span>
                </div>
            </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Mannol", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="Mannol" class="proizvoditel" id="squaredThree_Mannol" name="proizvoditel[]">
                    <label for="squaredThree_Mannol"></label>
                    <span>Mannol</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("ENEOS", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="ENEOS" class="proizvoditel" id="squaredThree_ENEOS" name="proizvoditel[]">
                        <label for="squaredThree_ENEOS"></label>
                        <span>ENEOS</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("KIXX", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="KIXX" class="proizvoditel" id="squaredThree_KIXX" name="proizvoditel[]">
                    <label for="squaredThree_KIXX"></label>
                    <span>KIXX</span>
                </div>
            </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("ZIC", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="ZIC" class="proizvoditel" id="squaredThree_ZIC" name="proizvoditel[]">
                    <label for="squaredThree_ZIC"></label>
                    <span>ZIC</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("YOKO", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="YOKO" class="proizvoditel" id="squaredThree_YOKO" name="proizvoditel[]">
                        <label for="squaredThree_YOKO"></label>
                        <span>YOKO</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Shell", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="Shell" class="proizvoditel" id="squaredThree_Shell" name="proizvoditel[]">
                    <label for="squaredThree_Shell"></label>
                    <span>Shell</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Газпромнефть", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="Газпромнефть" class="proizvoditel" id="squaredThree_Газпромнефть" name="proizvoditel[]">
                        <label for="squaredThree_Газпромнефть"></label>
                        <span>Газпромнефть</span>
                    </div>
                </li>
			<hr />
		</ul>
    <ul class="sidebar-catalogList">
        <li class="cataloglist_header">Тип двигателя:</li><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['dvigatel'])) echo (in_array("Бензиновый", $_GET['dvigatel']))?"checked":"";  ?> type="checkbox" value="Бензиновый" class="dvigatel" id="squaredThree_Бензиновый" name="dvigatel[]">
                <label for="squaredThree_Бензиновый"></label>
                <span>Бензиновый</span>
            </div>
        </li><br /><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['dvigatel'])) echo (in_array("Дизельный", $_GET['dvigatel']))?"checked":"";  ?>  type="checkbox" value="Дизельный" class="dvigatel" id="squaredThree_Дизельный" name="dvigatel[]">
                <label for="squaredThree_Дизельный"></label>
                <span>Дизельный</span>
            </div>
        </li><br /><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['dvigatel'])) echo (in_array("На газе", $_GET['dvigatel']))?"checked":"";  ?>  type="checkbox" value="На газе" class="dvigatel" id="squaredThree_На газе" name="dvigatel[]">
                <label for="squaredThree_На газе"></label>
                <span>На газе</span>
            </div>
        </li>			<hr>
    </ul>
    <ul class="sidebar-catalogList">
        <li class="cataloglist_header">Состав:</li><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['sostav'])) echo (in_array("Минеральное", $_GET['sostav']))?"checked":"";  ?> type="checkbox" value="Минеральное" class="sostav" id="squaredThree_Минеральное" name="sostav[]">
                <label for="squaredThree_Минеральное"></label>
                <span>Минеральное</span>
            </div>
        </li><br /><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['sostav'])) echo (in_array("Полусинтетическое", $_GET['sostav']))?"checked":"";  ?> type="checkbox" value="Полусинтетическое" class="sostav" id="squaredThree_Полусинтетическое" name="sostav[]">
                <label for="squaredThree_Полусинтетическое"></label>
                <span>Полусинтетическое</span>
            </div>
        </li><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['sostav'])) echo (in_array("Синтетическое", $_GET['sostav']))?"checked":"";  ?> type="checkbox" value="Синтетическое" class="sostav" id="squaredThree_Синтетическое" name="sostav[]">
                <label for="squaredThree_Синтетическое"></label>
                <span>Синтетическое</span>
            </div>
        </li>			<hr>
    </ul>
		<ul class="sidebar-catalogList">
			<?php/*
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Вязкость';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['vyazkost'])) && (in_array($bla, $_GET['vyazkost']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="vyazkost" id="squaredThree_' . $bla . '" name="vyazkost[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}*/
			?>
                <li class="cataloglist_header">Вязкость:</li>
                <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['vyazkost'])) echo (in_array("0W-20", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="0W-20" class="vyazkost" id="squaredThree_0W-20" name="vyazkost[]">
                        <label for="squaredThree_0W-20"></label>
                        <span>0W-20</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("10W-30", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="10W-30" class="vyazkost" id="squaredThree_10W-30" name="vyazkost[]">
                    <label for="squaredThree_10W-30"></label>
                    <span>10W-30</span>
                </div>
            </li>
                <li>
                    <div class="squaredThree cat_div">
                        <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("0W-30", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="0W-30" class="vyazkost" id="squaredThree_0W-30" name="vyazkost[]">
                        <label for="squaredThree_0W-30"></label>
                        <span>0W-30</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['vyazkost'])) echo (in_array("10W-40", $_GET['vyazkost']))?"checked":"";  ?>  type="checkbox" value="10W-40" class="vyazkost" id="squaredThree_10W-40" name="vyazkost[]">
                    <label for="squaredThree_10W-40"></label>
                    <span>10W-40</span>
                </div>
            </li>
                <li>
                    <div class="squaredThree cat_div">
                        <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("0W-40", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="0W-40" class="vyazkost" id="squaredThree_0W-40" name="vyazkost[]">
                        <label for="squaredThree_0W-40"></label>
                        <span>0W-40</span>
                    </div>
                </li>


                <li>
                    <div class="squaredThree cat_div">
                        <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("10W-60", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="10W-60" class="vyazkost" id="squaredThree_10W-60" name="vyazkost[]">
                        <label for="squaredThree_10W-60"></label>
                        <span>10W-60</span>
                    </div>
                </li>
                <li>
                    <div class="squaredThree cat_div">
                        <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("5W-20", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="5W-20" class="vyazkost" id="squaredThree_5W-20" name="vyazkost[]">
                        <label for="squaredThree_5W-20"></label>
                        <span>5W-20</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("15W-40", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="15W-40" class="vyazkost" id="squaredThree_15W-40" name="vyazkost[]">
                    <label for="squaredThree_15W-40"></label>
                    <span>15W-40</span>
                </div>
            </li>

            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['vyazkost'])) echo (in_array("5W-30", $_GET['vyazkost']))?"checked":"";  ?>  type="checkbox" value="5W-30" class="vyazkost" id="squaredThree_5W-30" name="vyazkost[]">
                        <label for="squaredThree_5W-30"></label>
                        <span>5W-30</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['vyazkost'])) echo (in_array("15W-50", $_GET['vyazkost']))?"checked":"";  ?>  type="checkbox" value="15W-50" class="vyazkost" id="squaredThree_15W-50" name="vyazkost[]">
                    <label for="squaredThree_15W-50"></label>
                    <span>15W-50</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['vyazkost'])) echo (in_array("5W-40", $_GET['vyazkost']))?"checked":"";  ?>  type="checkbox" value="5W-40" class="vyazkost" id="squaredThree_5W-40" name="vyazkost[]">
                        <label for="squaredThree_5W-40"></label>
                        <span>5W-40</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input  <?php  if(isset($_GET['vyazkost'])) echo (in_array("20W-50", $_GET['vyazkost']))?"checked":"";  ?> type="checkbox" value="20W-50" class="vyazkost" id="squaredThree_20W-50" name="vyazkost[]">
                    <label for="squaredThree_20W-50"></label>
                    <span>20W-50</span>
                </div>
            </li>

            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['vyazkost'])) echo (in_array("5W-50", $_GET['vyazkost']))?"checked":"";  ?>  type="checkbox" value="5W-50" class="vyazkost" id="squaredThree_5W-50" name="vyazkost[]">
                        <label for="squaredThree_5W-50"></label>
                        <span>5W-50</span>
                    </div>
                </li>
			<hr />
		</ul>
		<ul class="sidebar-catalogList">
			<?php/*
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Объем';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['obem'])) && (in_array($bla, $_GET['obem']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="obem" id="squaredThree_' . $bla . '" name="obem[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}*/
			?>
                <li class="cataloglist_header">Объем:</li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['obem'])) echo (in_array("1", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="1" class="obem" id="squaredThree_1" name="obem[]">
                        <label for="squaredThree_1"></label>
                        <span>1</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['obem'])) echo (in_array("5", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="5" class="obem" id="squaredThree_5" name="obem[]">
                    <label for="squaredThree_5"></label>
                    <span>5</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['obem'])) echo (in_array("3", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="3" class="obem" id="squaredThree_3" name="obem[]">
                        <label for="squaredThree_3"></label>
                        <span>3</span>
                    </div>
                </li>
            <li>
                <div class="squaredThree cat_div">
                    <input <?php  if(isset($_GET['obem'])) echo (in_array("6", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="6" class="obem" id="squaredThree_6" name="obem[]">
                    <label for="squaredThree_6"></label>
                    <span>6</span>
                </div>
            </li>
            <li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['obem'])) echo (in_array("4", $_GET['obem']))?"checked":"";  ?> type="checkbox" value="4" class="obem" id="squaredThree_4" name="obem[]">
                        <label for="squaredThree_4"></label>
                        <span>4</span>
                    </div>
                </li>
			<hr />
		</ul>
		<ul class="sidebar-catalogList">
			<?php/*
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Состав';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['sostav'])) && (in_array($bla, $_GET['sostav']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="sostav" id="squaredThree_' . $bla . '" name="sostav[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}*/
			?>
                <li class="cataloglist_header">Тактность двигателя:</li><li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['taktnistdrivatelya'])) echo (in_array("2-х тактный", $_GET['taktnistdrivatelya']))?"checked":"";  ?> type="checkbox" value="2-х тактный" class="taktnistdrivatelya" id="squaredThree_2-х тактный" name="taktnistdrivatelya[]">
                        <label for="squaredThree_2-х тактный"></label>
                        <span>2-х тактный</span>
                    </div>
                </li><br /><li>
                    <div class="squaredThree cat_div">
                        <input <?php  if(isset($_GET['taktnistdrivatelya'])) echo (in_array("4-х тактный", $_GET['taktnistdrivatelya']))?"checked":"";  ?> type="checkbox" value="4-х тактный" class="taktnistdrivatelya" id="squaredThree_4-х тактный" name="taktnistdrivatelya[]">
                        <label for="squaredThree_4-х тактный"></label>
                        <span>4-х тактный</span>
                    </div>
                </li>
			<hr />
		</ul>
			<?php/*
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Тактность двигателя';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['taktnistdrivatelya'])) && (in_array($bla, $_GET['taktnistdrivatelya']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="taktnistdrivatelya" id="squaredThree_' . $bla . '" name="taktnistdrivatelya[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}
			?>
			<?php
			$wpdb->postmeta;
			echo '<li class="cataloglist_header">';
			echo $metakey = 'Тип двигателя';
			echo ':</li>';
			$mm = $wpdb->get_col($wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ORDER BY meta_value ASC", $metakey) );
			if ($mm) {
				foreach ($mm as $bla) {
					$checked = '';
					if((isset($_GET['dvigatel'])) && (in_array($bla, $_GET['dvigatel']))) $checked = ' checked ';
					echo '<li>
							<div class="squaredThree cat_div">
								<input '.$checked.' type="checkbox" value="' . $bla . '" class="dvigatel" id="squaredThree_' . $bla . '" name="dvigatel[]" />
								<label for="squaredThree_' . $bla . '"></label>
								<span>' . $bla . '</span>
							</div>
					</li>';
				}
			}*/
			?>

	</div>
	<div id="primary" class="content-area col-md-8">
		<main id="main" class="site-main" role="main">
			<div class="header_title_products">
				<h3>Моторные масла</h3>
				<div class="sorting">
					<span>Сортировать по:</span>
					<select class="sorting" name="order" style="    border-radius: 5px;">
						<option value="none" <?php if($_GET['order']=="none") echo 'selected';?> >Приоритету</option>
						<option value="price_desc" <?php if($_GET['order']=="price_desc") echo 'selected';?> >Цене - убывание</option>
						<option value="price_asc" <?php if($_GET['order']=="price_asc") echo 'selected';?> >Цене - возростание</option>
						<option value="title_desc" <?php if($_GET['order']=="title_desc") echo 'selected';?> >Названию - Я-А Z-A</option>
						<option value="title_asc" <?php if($_GET['order']=="title_asc") echo 'selected';?> >Названию - A-Z А-Я</option>
					</select>
				</div>
			</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
				?>


			<?php endwhile; // end of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

</form>
<?php get_footer(); ?>
