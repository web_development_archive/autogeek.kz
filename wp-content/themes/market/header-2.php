<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Market
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>

<?php wp_head(); ?>
	<style>
		.advantage {
			position: relative;
			margin-top: 40px;
			height: 230px;
			border: solid 3px #ddd;
			border-radius: 10px;
		}
		.advantage h1 {
			font-weight: normal;
			font-size: 24px;
			text-transform: uppercase;
			position: absolute;
			padding: 0 10px;
			display: inline-block;
			left: 50%;
			top: -13px;
			margin-left: -101px;
		}
		.advantage-list {
			padding-top: 50px;
			text-align: center;
		}
		.advantage-list ul {
			display: inline-block;
		}
		.advantage-list li {
			float: left;
			width: 250px;
		}
		.advantage-img {
			height: 110px;
			overflow: hidden;
		}
		.advantage h2 {
			margin-top: 10px;
			font-size: 14px;
			font-weight: normal;
		}
		.advantage h2 a {
			text-transform: uppercase;
			color: #3c3c3c;
		}
		.shn ul li img {
			width: 24px;
		}
		.img img {
			width: 45px;
			margin: 0 10px;
		}
		.counters {
			width: auto!important;
			display: block;
			margin: 10px!important;
		}
		footer#colophon {
			display: none!important;
		}

	</style>
	<script>
	$(function() {
		alert(1);
		var count = 0;
		$("select#mobileMenu_menu-main_menu option").each(function (index) {
			if ($(this).attr("value") == "undefined") {
				alert(count);
				count++;
				if (count == 1) {
					$(this).attr("value", "http://autogeek.kz/%d0%b0%d0%b2%d1%82%d0%be-%d0%b0%d0%ba%d1%81%d0%b5%d1%81%d1%81%d1%83%d0%b0%d1%80%d1%8b/%d0%b0%d0%ba%d0%ba%d1%83%d0%bc%d1%83%d0%bb%d1%8f%d1%82%d0%be%d1%80%d1%8b/");
				} else if (count == 2) {
					$(this).attr("value", "http://autogeek.kz/%d0%bf%d0%be%d0%b4%d0%b1%d0%be%d1%80-%d0%bc%d0%b0%d1%81%d0%bb%d0%b0-%d0%b8-%d1%84%d0%b8%d0%bb%d1%8c%d1%82%d1%80%d0%be%d0%b2/");
				} else if (count == 3) {
					$(this).attr("value", "http://autogeek.kz/%d0%be%d0%bf%d1%82%d0%be%d0%b2%d1%8b%d0%bc-%d0%bf%d0%be%d0%ba%d1%83%d0%bf%d0%b0%d1%82%d0%b5%d0%bb%d1%8f%d0%bc/");
				} else
					}
				;


			}
			)
			;
		});
		</script>

</head>

<body <?php body_class(); ?>>
<div id="parallax-bg"></div>
<div id="page" class="hfeed site">
	<?php do_action( 'before' ); ?>
	<div id="top-bar">
		<div class="container">
			
			<div id="top-search-form" class="col-md-12">
				<?php get_template_part('searchform', 'top'); ?>
			</div>
			
			<div id="top-navigation" class="col-md-9">
				<?php wp_nav_menu( array( 'theme_location' => 'top' ) ); ?>
			</div>	

			 <div class="top-icons-container col-md-3">
				
				<?php if (class_exists('woocommerce')) :
				?>
					<div class="top-cart-icon">
						<i class="fa fa-shopping-cart"></i>
						<?php
						 global $woocommerce; ?>
	 
					<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d товар', '%d товаров', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></a>
					</div>
				<?php endif; ?>	
					<div class="top-search-icon">
						<i class="fa fa-search"></i>
					</div>
			</div>
		
		</div>
	</div><!--#top-bar-->
	
	<div id="header-top">
		<header id="masthead" class="site-header row container" role="banner">
			<div class="site-branding col-md-6 col-xs-12">
			<?php if((of_get_option('logo', true) != "") && (of_get_option('logo', true) != 1) ) { ?>
				<h1 class="site-title logo-container"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<?php
				echo "<img class='main_logo' src='".of_get_option('logo', true)."' title='".esc_attr(get_bloginfo( 'name','display' ) )."'></a></h1>";	
				}
			else { ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> 
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			<?php	
			}
			?>
			</div>	
			
			<?php
			
			if ( of_get_option('facebook',true) == 1 ) :
				 get_template_part('defaults/social');
			else :
				get_template_part('social', 'default');
			endif; ?>
			
		</header><!-- #masthead -->
	</div>
	
	<div id="header-2">
		<div class="container">
		<div class="default-nav-wrapper col-md-11 col-xs-12"> 	
		   <nav id="site-navigation" class="main-navigation" role="navigation">
	         <div id="nav-container">
				<h1 class="menu-toggle"></h1>
				<div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'market' ); ?>"><?php _e( 'Skip to content', 'market' ); ?></a></div>
	
				<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
	          </div>  
			</nav><!-- #site-navigation -->
		  </div>
		  
		<div id="top-search" class="col-md-1 col-xs-12">
			<?php // get_search_form(); ?>
		</div>
		</div>
	</div>
	
	<?php
			if ( of_get_option('slide1',true) == 1 ) :
				 get_template_part('defaults/slider');
			else :
				get_template_part('slider', 'nivo');
			endif; ?>
		<div id="content" class="site-content container row clearfix clear">
		<div class="col-md-12"> 
