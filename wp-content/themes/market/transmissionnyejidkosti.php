<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Market
 * Template Name: transmissionnyejidkosti
 */
get_header(); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".proizvoditel").change(function() {
			this.form.submit();
		});
		$(".obem").change(function() {
			this.form.submit();
		});
		$(".cvet").change(function() {
			this.form.submit();
		});
		$(".primemenie").change(function() {
			this.form.submit();
		});
		$(".vyazkost").change(function() {
			this.form.submit();
		});
		$(".sorting").change(function(){
			this.form.submit();
		});
		$(".taktnistdrivatelya").change(function() {
			this.form.submit();
		});
		$("#first_price_motmasla").change(function() {
			this.form.submit();
		});
		$("#second_price_motmasla").change(function() {
			$("#matormaslaidform").submit();
		});

		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 6600,
			stop: function( event, ui ) {
				$("#matormaslaidform").submit();
			},
			values: [ <?php if(isset($_GET['price_first'])) echo $_GET['price_first']; else echo 0; ?>, <?php if(isset($_GET['price_second'])) echo $_GET['price_second']; else echo 40000; ?> ],
			slide: function( event, ui ) {
				$( "#amount1" ).val(ui.values[ 0 ]+" тенге");
				$( "#amount2" ).val(ui.values[ 1 ]+" тенге");
				$( "#amount1_1" ).val(ui.values[ 0 ]);
				$( "#amount2_2" ).val(ui.values[ 1 ]);
			}
		});

		$( "#amount1" ).val($( "#slider-range" ).slider( "values", 0 )+" тенге");
		$( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 )+" тенге");
		$( "#amount1_1" ).val($( "#slider-range" ).slider( "values", 0 ));
		$( "#amount2_2" ).val($( "#slider-range" ).slider( "values", 1 ));
	});

</script>

<style>
	ul li {
		list-style: none;
	}
	header.entry-header {
		display: none;
	}
</style>
<form name="search" action="http://autogeek.kz/%D0%BC%D0%B0%D1%81%D0%BB%D0%B0-%D0%B8-%D0%B6%D0%B8%D0%B4%D0%BA%D0%BE%D1%81%D1%82%D0%B8/%D1%82%D1%80%D0%B0%D0%BD%D1%81%D0%BC%D0%B8%D1%81%D1%81%D0%B8%D0%BE%D0%BD%D0%BD%D1%8B%D0%B5-%D0%B6%D0%B8%D0%B4%D0%BA%D0%BE%D1%81%D1%82%D0%B8/" method="get" id="matormaslaidform">
<div id="" class="content-area col-md-4">

		<ul>
			<li class="cataloglist_header">Цена:</li>
			<li>
				<div class="interes_1 polzunok">
					<div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 23.3333333333333%; width: 46.6666666666667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 23.3333333333333%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 70%;"></span></div>
					<input name="price_first" type="hidden" id="amount1_1" readonly="" style="border:0;">
					<input name="price_second" type="hidden" id="amount2_2" readonly="" style="border:0;">
					<input type="text" id="amount1" readonly="" style="border:0;">
					<input type="text" id="amount2" readonly="" style="border:0;">
				</div>
			</li>
			<hr />
		</ul>
    <ul class="sidebar-catalogList">
        <li class="cataloglist_header">Производитель:</li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Mobil", $_GET['proizvoditel']))?"checked":"";  ?> type="checkbox" value="Mobil" class="proizvoditel" id="squaredThree_Mobil" name="proizvoditel[]">
                <label for="squaredThree_Mobil"></label>
                <span>Mobil</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Castrol", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="Castrol" class="proizvoditel" id="squaredThree_Castrol" name="proizvoditel[]">
                <label for="squaredThree_Castrol"></label>
                <span>Castrol</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("BP Visco", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="BP Visco" class="proizvoditel" id="squaredThree_BP Visco" name="proizvoditel[]">
                <label for="squaredThree_BP Visco"></label>
                <span>BP Visco</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("Mannol", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="Mannol" class="proizvoditel" id="squaredThree_Mannol" name="proizvoditel[]">
                <label for="squaredThree_Mannol"></label>
                <span>Mannol</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("ENEOS", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="ENEOS" class="proizvoditel" id="squaredThree_ENEOS" name="proizvoditel[]">
                <label for="squaredThree_ENEOS"></label>
                <span>ENEOS</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("KIXX", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="KIXX" class="proizvoditel" id="squaredThree_KIXX" name="proizvoditel[]">
                <label for="squaredThree_KIXX"></label>
                <span>KIXX</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("ZIC", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="ZIC" class="proizvoditel" id="squaredThree_ZIC" name="proizvoditel[]">
                <label for="squaredThree_ZIC"></label>
                <span>ZIC</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['proizvoditel'])) echo (in_array("YOKO", $_GET['proizvoditel']))?"checked":"";  ?>  type="checkbox" value="YOKO" class="proizvoditel" id="squaredThree_YOKO" name="proizvoditel[]">
                <label for="squaredThree_YOKO"></label>
                <span>YOKO</span>
            </div>
        </li>
        <hr />
    </ul>

    <ul class="sidebar-catalogList">
        <li class="cataloglist_header">Применение:</li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['primemenie'])) echo (in_array("Автоматическая КПП", $_GET['primemenie']))?"checked":"";  ?>  type="checkbox" value="Автоматическая КПП" class="primemenie" id="squaredThree_Автоматическая КПП" name="primemenie[]">
                <label for="squaredThree_Автоматическая КПП"></label>
                <span>Автоматическая КПП</span>
            </div>
        </li><br />
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['primemenie'])) echo (in_array("Механическая КПП", $_GET['primemenie']))?"checked":"";  ?>  type="checkbox" value="Механическая КПП" class="primemenie" id="squaredThree_Механическая КПП" name="primemenie[]">
                <label for="squaredThree_Механическая КПП"></label>
                <span>Механическая КПП</span>
            </div>
        </li><br />
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['primemenie'])) echo (in_array("КПП с двойным сцеплением", $_GET['primemenie']))?"checked":"";  ?>  type="checkbox" value="КПП с двойным сцеплением" class="primemenie" id="squaredThree_КПП с двойным сцеплением" name="primemenie[]">
                <label for="squaredThree_КПП с двойным сцеплением"></label>
                <span>КПП с двойным сцеплением</span>
            </div>
        </li><br />
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['primemenie'])) echo (in_array("ГУР", $_GET['primemenie']))?"checked":"";  ?>  type="checkbox" value="ГУР" class="primemenie" id="squaredThree_ГУР" name="primemenie[]">
                <label for="squaredThree_ГУР"></label>
                <span>ГУР</span>
            </div>
        </li><br />
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['primemenie'])) echo (in_array("Мосты и редукторы", $_GET['primemenie']))?"checked":"";  ?> type="checkbox" value="Мосты и редукторы" class="primemenie" id="squaredThree_Мосты и редукторы" name="primemenie[]">
                <label for="squaredThree_Мосты и редукторы"></label>
                <span>Мосты и редукторы</span>
            </div>
        </li>
        <hr>
    </ul>

    <ul class="sidebar-catalogList">
        <li class="cataloglist_header">Объем:</li><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['obem'])) echo (in_array("0,5", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="0,5" class="obem" id="squaredThree_0,5" name="obem[]">
                <label for="squaredThree_0,5"></label>
                <span>0,5</span>
            </div>
        </li><li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['obem'])) echo (in_array("1", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="1" class="obem" id="squaredThree_1" name="obem[]">
                <label for="squaredThree_1"></label>
                <span>1</span>
            </div>
        </li>
        <li>
            <div class="squaredThree cat_div">
                <input <?php  if(isset($_GET['obem'])) echo (in_array("4", $_GET['obem']))?"checked":"";  ?>  type="checkbox" value="4" class="obem" id="squaredThree_4" name="obem[]">
                <label for="squaredThree_4"></label>
                <span>4</span>
            </div>
        </li>
        <hr>
    </ul>

		
	</div>
	<div id="primary" class="content-area col-md-8">
		<main id="main" class="site-main" role="main">
			<div class="header_title_products">
				<h3>Трансмиссионные жидкости</h3>
				<div class="sorting">
					<span>Сортировать по:</span>
					<select class="sorting" name="order" style="    border-radius: 5px;">
						<option value="none" <?php if($_GET['order']=="none") echo 'selected';?> >Приоритету</option>
						<option value="price_desc" <?php if($_GET['order']=="price_desc") echo 'selected';?> >Цене - убывание</option>
						<option value="price_asc" <?php if($_GET['order']=="price_asc") echo 'selected';?> >Цене - возростание</option>
						<option value="title_desc" <?php if($_GET['order']=="title_desc") echo 'selected';?> >Названию - Я-А Z-A</option>
						<option value="title_asc" <?php if($_GET['order']=="title_asc") echo 'selected';?> >Названию - A-Z А-Я</option>
					</select>
				</div>
			</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
				?>


			<?php endwhile; // end of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

</form>
<?php get_footer(); ?>
