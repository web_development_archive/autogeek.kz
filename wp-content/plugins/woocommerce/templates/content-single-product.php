<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<div id="" class="content-area col-md-8">
	<main id="main" class="site-main" role="main">

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );

		global $post;

		$vyzkost = (get_post_meta($post->ID, 'Производитель', true));
		if($vyzkost!="") echo '<strong>Производитель:</strong> '.$vyzkost.'<br />';

		$vyzkost = (get_post_meta($post->ID, 'Вязкость', true));
		if($vyzkost!="") echo '<strong>Вязкость:</strong> '.$vyzkost.'<br />';

		$vyzkost = (get_post_meta($post->ID, 'Объем', true));
		if($vyzkost!="") echo '<strong>Объем:</strong> '.$vyzkost.'<br />';

		$vyzkost = (get_post_meta($post->ID, 'Состав', true));
		if($vyzkost!="") echo '<strong>Состав:</strong> '.$vyzkost.'<br />';

		$vyzkost = (get_post_meta($post->ID, 'Тактность двигателя', true));
		if($vyzkost!="") echo '<strong>Тактность двигателя:</strong> '.$vyzkost.'<br />';

		$vyzkost = (get_post_meta($post->ID, 'Тип двигателя', true));
		if($vyzkost!="") echo '<strong>Тип двигателя:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Цвет', true));
        if($vyzkost!="") echo '<strong>Цвет:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Длина, мм:', true));
        if($vyzkost!="") echo '<strong>Длина, мм:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Высота, мм:', true));
        if($vyzkost!="") echo '<strong>Высота, мм:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Ширина, мм:', true));
        if($vyzkost!="") echo '<strong>Ширина, мм:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Применение', true));
        if($vyzkost!="") echo '<strong>Применение:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Область применения', true));
        if($vyzkost!="") echo '<strong>Область применения:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Емкость, Ач:', true));
        if($vyzkost!="") echo '<strong>Емкость, Ач:</strong> '.$vyzkost.'<hr />';

        $vyzkost = (get_post_meta($post->ID, 'Вязкость', true));
        if($vyzkost!="") echo '<strong>Вязкость:</strong> '.$vyzkost.'<hr />';

		?>


	</div><!-- .summary -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->
	</main><!-- #main -->
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php do_action( 'woocommerce_after_single_product' ); ?>
